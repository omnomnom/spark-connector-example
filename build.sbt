organization := "me.lazyval"

name := "spark-connector-example"

scalaVersion := "2.10.4"

scalacOptions ++= Seq("-unchecked", "-deprecation", "-Xlint")

libraryDependencies ++= Seq(
  "org.apache.spark" % "spark-core_2.10" % "1.0.2",
  // I've took below definition from spark-cassandra-connector readme page: https://github.com/datastax/spark-cassandra-connector
  "com.datastax.spark" % "spark-cassandra-connector_2.10" % "1.0.0-rc4" withSources() withJavadoc()
)

initialCommands in console := "import me.lazyval._"
